package org.example;
import org.example.entity.Rectangle;
import org.example.entity.Square;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        /* RESULTATS DES PREMIERS EXERCICES (diapos 104-105)
        reverse("Trois petits chats");
        System.out.println(pyramide('*'));
        sumNumbers();
        evenOdd();
        guessNumber();
        countWords();
        downloadFile();
        readFile();
        */

        Rectangle rectangle = new Rectangle(5,3);
        rectangle.display();
        Square carre = new Square(3);
        System.out.println("Surface du carré : " + carre.calculSurface(carre.getCote()));
    }

    // Renverser une chaine de caractères
    public static void reverse(String chaine) {
        String result = "";
        for (int i = chaine.length() - 1; i >= 0; i--) {
            result += chaine.charAt(i);
        }
        System.out.println(result);
    }

    // Afficher une pyramide à l’aide d’un caractère
    public static char pyramide(char caractere) {
        for (int ligne = 1; ligne <= 5; ligne++) {
            for (int space = 1; space <= 5 - ligne; space++) {
                System.out.print(" ");
            }

            for (int i = 1; i <= 2 * ligne - 1; i++) {
                System.out.print(caractere);
            }
            System.out.println();
        }
        return ' ';
    }

    // Calculer la somme des nombres jusqu’à 10 000
    public static int sumNumbers() {
        int count = 0;
        for (int i = 0; i <= 10000; i++) {
            count++;
        }
        System.out.println(count);
        return count;
    }

    // Vérifier si un nombre entré est pair, impair, premier
    public static void evenOdd() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Entrez un nombre entier : ");
        int num = scanner.nextInt();
        scanner.close();

        boolean estPremier = true;
        String result = String.valueOf(num);
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                estPremier = false;
            }
        }
        if (estPremier) {
            result += " est un nombre premier";
        } else {
            result += " n'est pas un nombre premier";
        }
        if (num % 2 == 0) {
            result += " et est un nombre pair";
        } else {
            result += " et est un nombre impair";
        }
        System.out.println(result);
    }

    // trouver un nombre aléatoire choisi par l’ordinateur
    public static void guessNumber() {
        Random random = new Random();
        int generatedNum = random.nextInt(100) + 1;

        int num = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Entrez un nombre entier : ");
        num = scanner.nextInt();
        int essai = 1;

        while (num != generatedNum) {
            if (num > generatedNum) {
                System.out.println("Plus petit");
            } else {
                System.out.println("Plus grand");
            }
            essai++;
            System.out.print("Entrez un nombre entier : ");
            num = scanner.nextInt();
        }
        System.out.println("Gagné en " + essai + " essai(s)");
        scanner.close();
    }

    //    Compter le nombre de mots dans une phrase
    public static void countWords() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ecrivez une phrase : ");
        String str = scanner.nextLine();
        String[] words = str.trim().split("\\s+");
        int nbWords = words.length;
        System.out.println("La phrase contient " + nbWords + " mots");
        scanner.close();
    }

    //    Sauvegarder votre phrase dans un fichier
    public static void downloadFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Écrivez une phrase à enregistrer : ");
        String str = scanner.nextLine();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("phrase.txt");

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(str);
            bufferedWriter.close();
            System.out.println("La phrase a bien été enregistrée");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scanner.close();
    }

//    Lire du texte dans un fichier et compter le nombre de mots contenu
    public static void readFile(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Écrire le chemin du fichier à lire : ");
        String path = scanner.nextLine();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String text = bufferedReader.readLine();
            String[] words = text.trim().split("\\s+");
            int nbWords = words.length;
            System.out.println("La phrase contient " + nbWords + " mots");
        } catch (FileNotFoundException e) {
            System.err.println("Le fichier n'a pas pu être trouvé");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }
}
