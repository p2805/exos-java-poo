package org.example.entity;

public class Rectangle {
    private int longueur;
    private int largeur;

    public Rectangle(int longueur, int largeur) {
        this.longueur = longueur;
        this.largeur = largeur;
    }

    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public void display() {
        for (int i = 0; i < largeur; i++) {
            for (int j = 0; j < longueur; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
