package org.example.entity;

public class Square extends Rectangle {

    public Square(int cote) {
        super(cote, cote);
    }

    public int getCote() {
        return getLargeur();
    }

    public void setCote(int cote) {
        setLongueur(cote);
    }

    public int calculSurface(int cote) {
        return cote * cote;
    }

}
